﻿namespace CalculatorLV
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button_7 = new System.Windows.Forms.Button();
            this.button_8 = new System.Windows.Forms.Button();
            this.button_4 = new System.Windows.Forms.Button();
            this.button_9 = new System.Windows.Forms.Button();
            this.button_5 = new System.Windows.Forms.Button();
            this.button_clr = new System.Windows.Forms.Button();
            this.button_6 = new System.Windows.Forms.Button();
            this.button_div = new System.Windows.Forms.Button();
            this.button_off = new System.Windows.Forms.Button();
            this.button_add = new System.Windows.Forms.Button();
            this.button_mul = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.button_log = new System.Windows.Forms.Button();
            this.button_equal = new System.Windows.Forms.Button();
            this.button_sub = new System.Windows.Forms.Button();
            this.button_dot = new System.Windows.Forms.Button();
            this.button_3 = new System.Windows.Forms.Button();
            this.button_0 = new System.Windows.Forms.Button();
            this.button_2 = new System.Windows.Forms.Button();
            this.button_1 = new System.Windows.Forms.Button();
            this.button_sin = new System.Windows.Forms.Button();
            this.button_cos = new System.Windows.Forms.Button();
            this.button_tan = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(44, 44);
            this.textBox1.MinimumSize = new System.Drawing.Size(0, 35);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(362, 22);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button_7
            // 
            this.button_7.Location = new System.Drawing.Point(44, 87);
            this.button_7.Name = "button_7";
            this.button_7.Size = new System.Drawing.Size(55, 53);
            this.button_7.TabIndex = 1;
            this.button_7.Text = "7";
            this.button_7.UseVisualStyleBackColor = true;
            this.button_7.Click += new System.EventHandler(this.button_7_Click);
            // 
            // button_8
            // 
            this.button_8.Location = new System.Drawing.Point(105, 88);
            this.button_8.Name = "button_8";
            this.button_8.Size = new System.Drawing.Size(55, 52);
            this.button_8.TabIndex = 2;
            this.button_8.Text = "8";
            this.button_8.UseVisualStyleBackColor = true;
            this.button_8.Click += new System.EventHandler(this.button_8_Click);
            // 
            // button_4
            // 
            this.button_4.Location = new System.Drawing.Point(44, 146);
            this.button_4.Name = "button_4";
            this.button_4.Size = new System.Drawing.Size(55, 53);
            this.button_4.TabIndex = 3;
            this.button_4.Text = "4";
            this.button_4.UseVisualStyleBackColor = true;
            this.button_4.Click += new System.EventHandler(this.button_4_Click);
            // 
            // button_9
            // 
            this.button_9.Location = new System.Drawing.Point(166, 87);
            this.button_9.Name = "button_9";
            this.button_9.Size = new System.Drawing.Size(57, 52);
            this.button_9.TabIndex = 4;
            this.button_9.Text = "9";
            this.button_9.UseVisualStyleBackColor = true;
            this.button_9.Click += new System.EventHandler(this.button_9_Click);
            // 
            // button_5
            // 
            this.button_5.Location = new System.Drawing.Point(105, 146);
            this.button_5.Name = "button_5";
            this.button_5.Size = new System.Drawing.Size(55, 53);
            this.button_5.TabIndex = 5;
            this.button_5.Text = "5";
            this.button_5.UseVisualStyleBackColor = true;
            this.button_5.Click += new System.EventHandler(this.button_5_Click);
            // 
            // button_clr
            // 
            this.button_clr.Location = new System.Drawing.Point(229, 88);
            this.button_clr.Name = "button_clr";
            this.button_clr.Size = new System.Drawing.Size(55, 53);
            this.button_clr.TabIndex = 6;
            this.button_clr.Text = "CLR";
            this.button_clr.UseVisualStyleBackColor = true;
            this.button_clr.Click += new System.EventHandler(this.button_clr_Click);
            // 
            // button_6
            // 
            this.button_6.Location = new System.Drawing.Point(168, 145);
            this.button_6.Name = "button_6";
            this.button_6.Size = new System.Drawing.Size(55, 53);
            this.button_6.TabIndex = 7;
            this.button_6.Text = "6";
            this.button_6.UseVisualStyleBackColor = true;
            this.button_6.Click += new System.EventHandler(this.button_6_Click);
            // 
            // button_div
            // 
            this.button_div.Location = new System.Drawing.Point(290, 147);
            this.button_div.Name = "button_div";
            this.button_div.Size = new System.Drawing.Size(55, 53);
            this.button_div.TabIndex = 8;
            this.button_div.Text = "/";
            this.button_div.UseVisualStyleBackColor = true;
            this.button_div.Click += new System.EventHandler(this.button_div_Click);
            // 
            // button_off
            // 
            this.button_off.Location = new System.Drawing.Point(290, 88);
            this.button_off.Name = "button_off";
            this.button_off.Size = new System.Drawing.Size(55, 53);
            this.button_off.TabIndex = 9;
            this.button_off.Text = "OFF";
            this.button_off.UseVisualStyleBackColor = true;
            this.button_off.Click += new System.EventHandler(this.button_off_Click);
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(229, 145);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(55, 53);
            this.button_add.TabIndex = 11;
            this.button_add.Text = "+";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_mul
            // 
            this.button_mul.Location = new System.Drawing.Point(290, 262);
            this.button_mul.Name = "button_mul";
            this.button_mul.Size = new System.Drawing.Size(55, 53);
            this.button_mul.TabIndex = 21;
            this.button_mul.Text = "*";
            this.button_mul.UseVisualStyleBackColor = true;
            this.button_mul.Click += new System.EventHandler(this.button_mul_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.Location = new System.Drawing.Point(290, 205);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(55, 53);
            this.button_sqrt.TabIndex = 20;
            this.button_sqrt.Text = "sqrt";
            this.button_sqrt.UseVisualStyleBackColor = true;
            this.button_sqrt.Click += new System.EventHandler(this.button_sqrt_Click);
            // 
            // button_log
            // 
            this.button_log.Location = new System.Drawing.Point(351, 262);
            this.button_log.Name = "button_log";
            this.button_log.Size = new System.Drawing.Size(55, 53);
            this.button_log.TabIndex = 19;
            this.button_log.Text = "log";
            this.button_log.UseVisualStyleBackColor = true;
            this.button_log.Click += new System.EventHandler(this.button_log_Click);
            // 
            // button_equal
            // 
            this.button_equal.Location = new System.Drawing.Point(168, 262);
            this.button_equal.Name = "button_equal";
            this.button_equal.Size = new System.Drawing.Size(116, 53);
            this.button_equal.TabIndex = 18;
            this.button_equal.Text = "=";
            this.button_equal.UseVisualStyleBackColor = true;
            this.button_equal.Click += new System.EventHandler(this.button_equal_Click);
            // 
            // button_sub
            // 
            this.button_sub.Location = new System.Drawing.Point(229, 205);
            this.button_sub.Name = "button_sub";
            this.button_sub.Size = new System.Drawing.Size(55, 53);
            this.button_sub.TabIndex = 17;
            this.button_sub.Text = "-";
            this.button_sub.UseVisualStyleBackColor = true;
            this.button_sub.Click += new System.EventHandler(this.button_sub_Click);
            // 
            // button_dot
            // 
            this.button_dot.Location = new System.Drawing.Point(105, 263);
            this.button_dot.Name = "button_dot";
            this.button_dot.Size = new System.Drawing.Size(55, 53);
            this.button_dot.TabIndex = 16;
            this.button_dot.Text = ".";
            this.button_dot.UseVisualStyleBackColor = true;
            this.button_dot.Click += new System.EventHandler(this.button_dot_Click);
            // 
            // button_3
            // 
            this.button_3.Location = new System.Drawing.Point(166, 204);
            this.button_3.Name = "button_3";
            this.button_3.Size = new System.Drawing.Size(57, 52);
            this.button_3.TabIndex = 15;
            this.button_3.Text = "3";
            this.button_3.UseVisualStyleBackColor = true;
            this.button_3.Click += new System.EventHandler(this.button_3_Click);
            // 
            // button_0
            // 
            this.button_0.Location = new System.Drawing.Point(44, 263);
            this.button_0.Name = "button_0";
            this.button_0.Size = new System.Drawing.Size(55, 53);
            this.button_0.TabIndex = 14;
            this.button_0.Text = "0";
            this.button_0.UseVisualStyleBackColor = true;
            this.button_0.Click += new System.EventHandler(this.button_0_Click);
            // 
            // button_2
            // 
            this.button_2.Location = new System.Drawing.Point(105, 205);
            this.button_2.Name = "button_2";
            this.button_2.Size = new System.Drawing.Size(55, 52);
            this.button_2.TabIndex = 13;
            this.button_2.Text = "2";
            this.button_2.UseVisualStyleBackColor = true;
            this.button_2.Click += new System.EventHandler(this.button_2_Click);
            // 
            // button_1
            // 
            this.button_1.Location = new System.Drawing.Point(44, 204);
            this.button_1.Name = "button_1";
            this.button_1.Size = new System.Drawing.Size(55, 53);
            this.button_1.TabIndex = 12;
            this.button_1.Text = "1";
            this.button_1.UseVisualStyleBackColor = true;
            this.button_1.Click += new System.EventHandler(this.button20_Click);
            // 
            // button_sin
            // 
            this.button_sin.Location = new System.Drawing.Point(351, 88);
            this.button_sin.Name = "button_sin";
            this.button_sin.Size = new System.Drawing.Size(55, 53);
            this.button_sin.TabIndex = 22;
            this.button_sin.Text = "sin";
            this.button_sin.UseVisualStyleBackColor = true;
            this.button_sin.Click += new System.EventHandler(this.button_sin_Click);
            // 
            // button_cos
            // 
            this.button_cos.Location = new System.Drawing.Point(351, 147);
            this.button_cos.Name = "button_cos";
            this.button_cos.Size = new System.Drawing.Size(55, 53);
            this.button_cos.TabIndex = 23;
            this.button_cos.Text = "cos";
            this.button_cos.UseVisualStyleBackColor = true;
            this.button_cos.Click += new System.EventHandler(this.button_cos_Click);
            // 
            // button_tan
            // 
            this.button_tan.Location = new System.Drawing.Point(351, 206);
            this.button_tan.Name = "button_tan";
            this.button_tan.Size = new System.Drawing.Size(55, 53);
            this.button_tan.TabIndex = 24;
            this.button_tan.Text = "tan";
            this.button_tan.UseVisualStyleBackColor = true;
            this.button_tan.Click += new System.EventHandler(this.button_tan_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 450);
            this.Controls.Add(this.button_tan);
            this.Controls.Add(this.button_cos);
            this.Controls.Add(this.button_sin);
            this.Controls.Add(this.button_mul);
            this.Controls.Add(this.button_sqrt);
            this.Controls.Add(this.button_log);
            this.Controls.Add(this.button_equal);
            this.Controls.Add(this.button_sub);
            this.Controls.Add(this.button_dot);
            this.Controls.Add(this.button_3);
            this.Controls.Add(this.button_0);
            this.Controls.Add(this.button_2);
            this.Controls.Add(this.button_1);
            this.Controls.Add(this.button_add);
            this.Controls.Add(this.button_off);
            this.Controls.Add(this.button_div);
            this.Controls.Add(this.button_6);
            this.Controls.Add(this.button_clr);
            this.Controls.Add(this.button_5);
            this.Controls.Add(this.button_9);
            this.Controls.Add(this.button_4);
            this.Controls.Add(this.button_8);
            this.Controls.Add(this.button_7);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button_7;
        private System.Windows.Forms.Button button_8;
        private System.Windows.Forms.Button button_4;
        private System.Windows.Forms.Button button_9;
        private System.Windows.Forms.Button button_5;
        private System.Windows.Forms.Button button_clr;
        private System.Windows.Forms.Button button_6;
        private System.Windows.Forms.Button button_div;
        private System.Windows.Forms.Button button_off;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_mul;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Button button_log;
        private System.Windows.Forms.Button button_equal;
        private System.Windows.Forms.Button button_sub;
        private System.Windows.Forms.Button button_dot;
        private System.Windows.Forms.Button button_3;
        private System.Windows.Forms.Button button_0;
        private System.Windows.Forms.Button button_2;
        private System.Windows.Forms.Button button_1;
        private System.Windows.Forms.Button button_sin;
        private System.Windows.Forms.Button button_cos;
        private System.Windows.Forms.Button button_tan;
    }
}

